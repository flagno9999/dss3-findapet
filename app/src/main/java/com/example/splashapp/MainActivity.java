package com.example.splashapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    MediaPlayer mp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mp = MediaPlayer.create(this, R.raw.button_click);
    }

    public void goBuscar(View view) {
        mp.start();
        try {
            Intent intent = new Intent().setClass(MainActivity.this,search.class);
            startActivity(intent);
        }catch (Exception e){
            System.out.println("ERROR: "+e);
        }

    }
    public void goReportar(View view) {
        mp.start();
        try {
            Intent intent = new Intent().setClass(MainActivity.this,report.class);
            startActivity(intent);
        }catch (Exception e){
            System.out.println("ERROR: "+e);
        }
    }
    public void goFacebook(View view) {
        mp.start();
        try {
            Intent intent = getPackageManager().getLaunchIntentForPackage("com.facebook.katana");
            startActivity(intent);
        }catch (Exception e){
            System.out.println("ERROR: "+e);
        }
    }
    public void goYoutube(View view) {
        mp.start();
        try {
            Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.youtube");
            startActivity(intent);
        }catch (Exception e){
            System.out.println("ERROR: "+e);
        }
    }
}